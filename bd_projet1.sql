-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Listage de la structure de la table bd_projet1. competences
CREATE TABLE IF NOT EXISTS `competences` (
  `id_competences` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `langage` varchar(50) NOT NULL,
  PRIMARY KEY (`id_competences`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.competences : ~3 rows (environ)
/*!40000 ALTER TABLE `competences` DISABLE KEYS */;
INSERT INTO `competences` (`id_competences`, `langage`) VALUES
	(1, 'html'),
	(2, 'css'),
	(3, 'javascript'),
	(4, 'php');
/*!40000 ALTER TABLE `competences` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. consulter
CREATE TABLE IF NOT EXISTS `consulter` (
  `id_consulter` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `valider` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `id_developpeur` int(11) unsigned NOT NULL,
  `id_offre` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_consulter`),
  KEY `FK_consulter_developpeur` (`id_developpeur`),
  KEY `FK_consulter_offre` (`id_offre`),
  CONSTRAINT `FK_consulter_developpeur` FOREIGN KEY (`id_developpeur`) REFERENCES `developpeur` (`id_developpeur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_consulter_offre` FOREIGN KEY (`id_offre`) REFERENCES `offre` (`id_offre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.consulter : ~0 rows (environ)
/*!40000 ALTER TABLE `consulter` DISABLE KEYS */;
/*!40000 ALTER TABLE `consulter` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. developpeur
CREATE TABLE IF NOT EXISTS `developpeur` (
  `id_developpeur` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `courriel` varchar(50) NOT NULL,
  `telephone` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  PRIMARY KEY (`id_developpeur`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.developpeur : ~0 rows (environ)
/*!40000 ALTER TABLE `developpeur` DISABLE KEYS */;
INSERT INTO `developpeur` (`id_developpeur`, `nom`, `prenom`, `courriel`, `telephone`, `username`, `mdp`) VALUES
	(1, 'chocapic', 'zfef', 'zefzefzef', 2757575, 'test', 'test'),
	(2, 'aze', 'aze', 'aze', 6, 'aze', 'aze'),
	(3, 'a', 'a', 'a', 4, 'a', 'a');
/*!40000 ALTER TABLE `developpeur` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. developpeur_competence
CREATE TABLE IF NOT EXISTS `developpeur_competence` (
  `id_developpeur` int(11) unsigned NOT NULL,
  `id_competences` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_developpeur`,`id_competences`),
  KEY `FK_developpeur_competence_competences` (`id_competences`),
  CONSTRAINT `FK_developpeur_competence_competences` FOREIGN KEY (`id_competences`) REFERENCES `competences` (`id_competences`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_developpeur_competence_developpeur` FOREIGN KEY (`id_developpeur`) REFERENCES `developpeur` (`id_developpeur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.developpeur_competence : ~0 rows (environ)
/*!40000 ALTER TABLE `developpeur_competence` DISABLE KEYS */;
/*!40000 ALTER TABLE `developpeur_competence` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. entreprise
CREATE TABLE IF NOT EXISTS `entreprise` (
  `id_entreprise` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_entreprise` varchar(50) NOT NULL,
  `siret` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `mdp` varchar(50) NOT NULL,
  PRIMARY KEY (`id_entreprise`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.entreprise : ~7 rows (environ)
/*!40000 ALTER TABLE `entreprise` DISABLE KEYS */;
INSERT INTO `entreprise` (`id_entreprise`, `nom_entreprise`, `siret`, `username`, `mdp`) VALUES
	(1, 'chocapic', '121', 'test', 'test'),
	(2, 'chocapic', '56655665', 'chocolat', 'chocolat'),
	(3, '', '', '', ''),
	(4, '', '', '', ''),
	(5, '', '', '', ''),
	(6, '', '', '', ''),
	(7, 'chocapic', '56655665', 'test', 'test'),
	(8, '', '', '', '');
/*!40000 ALTER TABLE `entreprise` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. fichier
CREATE TABLE IF NOT EXISTS `fichier` (
  `id_fichier` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_fichier` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `id_offre` int(11) unsigned DEFAULT NULL,
  `id_consulter` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_fichier`),
  KEY `FK_fichier_offre` (`id_offre`),
  KEY `FK_fichier_consulter` (`id_consulter`),
  CONSTRAINT `FK_fichier_consulter` FOREIGN KEY (`id_consulter`) REFERENCES `consulter` (`id_consulter`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_fichier_offre` FOREIGN KEY (`id_offre`) REFERENCES `offre` (`id_offre`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.fichier : ~0 rows (environ)
/*!40000 ALTER TABLE `fichier` DISABLE KEYS */;
/*!40000 ALTER TABLE `fichier` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. messagerie
CREATE TABLE IF NOT EXISTS `messagerie` (
  `id_message` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `message` varchar(1000) NOT NULL,
  `id_developpeur` int(11) unsigned NOT NULL,
  `id_entreprise` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `FK_messagerie_developpeur` (`id_developpeur`),
  KEY `FK_messagerie_entreprise` (`id_entreprise`),
  CONSTRAINT `FK_messagerie_developpeur` FOREIGN KEY (`id_developpeur`) REFERENCES `developpeur` (`id_developpeur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_messagerie_entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.messagerie : ~0 rows (environ)
/*!40000 ALTER TABLE `messagerie` DISABLE KEYS */;
/*!40000 ALTER TABLE `messagerie` ENABLE KEYS */;

-- Listage de la structure de la table bd_projet1. offre
CREATE TABLE IF NOT EXISTS `offre` (
  `id_offre` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nom_offre` varchar(50) NOT NULL,
  `description_offre` varchar(50) NOT NULL,
  `id_entreprise` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_offre`),
  KEY `FK_OFFRE_entreprise` (`id_entreprise`),
  CONSTRAINT `FK_OFFRE_entreprise` FOREIGN KEY (`id_entreprise`) REFERENCES `entreprise` (`id_entreprise`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Listage des données de la table bd_projet1.offre : ~0 rows (environ)
/*!40000 ALTER TABLE `offre` DISABLE KEYS */;
/*!40000 ALTER TABLE `offre` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
