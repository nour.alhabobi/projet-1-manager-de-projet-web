<?php
  

    //si ce tableau pas c'est qu'on a au moins un fichier
    if(!empty($_FILES)){
        $file_name = $_FILES['fichier']['name'];
        $file_extension = strrchr($file_name, ".");

        $extension_autorisees = array('.pdf', '.PDF');
        
        //est ce que cette valeur fait partit du tableau (le pdf)
        if(in_array($file_extension, $extension_autorisees)){


        } else {
            echo 'Seuls les fichiers PDF sont autorisés';
        }

    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Déposer une offre</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/interface.css" />
    <!-- <script src="main.js"></script> -->
</head>
<body>

<form action = "/deposer/offre/entreprise" method = "post" enctype = "multipart/form-data">
        <div class = "page_entiere">
                

                <div class = "cote_gauche">

                <a href="/ajouter/nouvelle/offre">Déposer une offre</a>

                </div>


                <div class = "cote_droit">
                        
                        <h1><center> Déposer une nouvelle offre </center></h1>

                        </br>
                        <div class="colorTitle">
                            <label for="nom">Nom de l'offre :</label>
                            <input type="text" name="nom" id="nom"> 
                        </div>

                        </br>

                        <div class="colorTitle">
                            Description : <TEXTAREA name="description" rows=4 cols=40>Ecriver votre message... </TEXTAREA>
                        </div>

                        </br>
                        <!--
                        <div class="colorTitle">
                            <label for="fichier">Joindre un document</label>
                            <input type="FILE" name="nom" id="nom"> 
                        </div>
                        
                        <div>
                        <label for="upload">Choose a profile picture:</label>

                            <input type="file"
                                id="upload" name="upload"
                                accept="image/png, image/jpeg, application/pdf">
                        </div>
                        -->

                        <div>
                            <input type="file" name="fichier" id="fichier"> 
                            
                        </div>
                        </br> 
                        <div>
                            <input type="submit" value="Valider l'ajout de cet offre"> 
                            <input type="reset" value="Annuler">
                        </div>



                        
                
                </div>


        </div>
</form>
    
</body>
</html>