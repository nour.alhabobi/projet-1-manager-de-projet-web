<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Connection</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/connexion.css" />
    
    <script type="text/javascript">
    
    function verif()
{
	var usernameok=false;
	var mdpok=false;

	if(( document.getElementById("username") != undefined ) && (document.getElementById("username").value != ""))/* on verifie que le nom est bien indiqué*/
	{
		usernameok = true;
	}
	
	if(( document.getElementById("mdp") != undefined ) && (document.getElementById("mdp").value != ""))/* on verifie que le nom est bien indiqué*/
	{
		mdpok = true;
	}
	
	 
	if( usernameok && mdpok ){		
		return 	true;		
	}else{
		var msg = "";
		if( !usernameok ){ 
			msg += "Veuillez indiquer votre login\n";
		}
		if( !mdpok){
			msg +="Veuillez indiquez votre mot de passe\n";
		}
		
		alert (msg);
		return false;
	}
}
    
    
    
    
    </script>
</head>

<body class="bodyConnexion">

    <form action="connexion" method = "post" onSubmit="return verif()" >
    
        <div>

            <center>

                <div id="authentification">
                    
                    <h1>
                        <center>
                            <div class="colorTitle"> Connection Entreprise/Développeur 
                        </center>
                    </h1>

                    <div>
                        <p class="colorTitle">Entrez votre nom d'utilisateur et votre Mot de passe </p>
                    </div>

                    <div class="colorTitle">
                            
                            <label for="username"> Nom d'utilisateur : </label>
                            <input type="text" name = "username" id = "username">

                    </div>

                    <div>

                        <label class="colorTitle">Mot de passe :</label>
                        <input type = "text" name = "mdp" id = "mdp" />

                    </div>

                    <div> 

                        <input type = "submit" value = "Valider" id = "valider"/> 
                        <input type = "reset" value = "Annuler"/>

                    </div>

                    <div>

                        <a href = "inscription/entreprise">S'inscrire en tant que entreprise</a>

                    </div>

                    <a href="inscription/developpeur">S'inscrire en tant que développeur</a>


            </center>

        </div>

    </form>

</body>
</html>



