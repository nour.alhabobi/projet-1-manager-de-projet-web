<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>inscription d'une entreprise</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/inscription.css" />
</head>
<body class = "bodyInscription">

     <form id="form_inscription" action="/validation/inscription/developpeur" method = "post" >

            <center>
                
                <div id="titrePrincipale">
                    <p>S'inscrire en tant que Développeur<p>
                </div>
                <p>
                <div id="colorTitle">
                    <label for="nom">Nom développeur :</label>
                    <input type="text" name="nom" id="nom"> 
                </div>
                <p>
                <div id="colorTitle">
                    <label for="siret">Prénom du développeur :</label>
                    <input type="text" name="prenom" id="prenom">
                </div>
                <p>
                <div id="colorTitle">
                    <label for="username">courriel :</label>
                    <input type="text" name="courriel" id="courriel">
                </div>
                <p>

                <div id="colorTitle">
                    <label for="mdp">Téléphone :</label>
                    <input type="text" name="telephone" id="telephone">
                </div>
                <p>
                <div id="colorTitle">
                    <label for="mdp">Nom d'utilisateur :</label>
                    <input type="text" name="username" id="username">
                </div>
                <p>
                <div id="colorTitle">
                    <label for="mdp">Mot de passe :</label>
                    <input type="text" name="mdp" id="mdp">
                </div>
                
                <p>
                
                <div>
                    <input type="submit" value="Valider Inscription"> 
                    <input type="reset" value="Annuler l'inscription">
                </div>
                <p>
                <a href='/'>Retourner vers la page de connection</a>

            </center>
    </form>

        <!-- Une fois que le contenu html est chargé, on peut charger notre javascript -->
    <script type="text/javascript" src="/js/form_validation_inscription_developpeur.js"></script>
</body>
</html>
