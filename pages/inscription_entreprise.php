<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>inscription d'une entreprise</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/inscription.css" />
    <script src="main.js"></script>
</head>
<body class = "bodyInscription">

     <form id="form_inscription" action="/validation/inscription/entreprise" method = "post">

            <center>


                
                <div id="titrePrincipale">
                    <p>S'inscrire en tant que entreprise<p>
                </div>
                <p>
                <div id="colorTitle">
                    <label for="nom">Nom de l'entreprise :</label>
                    <input type="text" name="nom" id="nom"> 
                </div>
                <p>
                <div id="colorTitle">
                    <label for="siret">N°SIRET :</label>
                    <input type="text" name="siret" id="siret">
                </div>
                <p>

                <div id="colorTitle">
                    <label for="username">Créer un Nom d'utilisateur :</label>
                    <input type="text" name="username" id="username">
                </div>
                <p>


                <div id="colorTitle">
                    <label for="mdp">Créer un mot de passe :</label>
                    <input type="text" name="mdp" id="mdp">
                </div>
                
                <p>

                
                <div>
                    <input type="submit" value="Valider Inscription"> 
                    <input type="reset" value="Annuler l'inscription">
                </div>
               
                <a href='/'>Retourner vers la page de connection</a>

            </center>
    </form>

    <!-- Une fois que le contenu html est chargé, on peut charger notre javascript -->
    <script type="text/javascript" src="/js/form_validation_inscription_entreprise.js"></script>
</body>
</html>


