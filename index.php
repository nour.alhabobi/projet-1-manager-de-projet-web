<?php
require 'CONFIG.php';

// Librairie Flight permettant le routing
require 'flight/Flight.php';

// Appel du script de connexion au serveur et à la base de données
require 'database/Bdd.php';

/************************** */
// Gestion des routes de pages
/************************** */
Flight::route('/', function(){

    require 'pages/home.php';

});

Flight::route('/inscription/entreprise', function(){

    require 'pages/inscription_entreprise.php';

});

Flight::route('/inscription/developpeur', function(){

    require 'pages/inscription_developpeur.php';

});

Flight::route('/profil/entreprise', function() {
    
    require 'pages/interface_entreprise.php';
    
});

Flight::route('/deposer/offre/entreprise', function() {
    
    require 'pages/deposer_offre.php';
    
});



/***************************** */
// Gestion des routes de requêtes
/***************************** */


Flight::route('POST /connexion', function(){

    require 'requetes/identification_entreprise_developpeur.php';
    
});

//exécution requete du formulaire d'inscription coté entreprise
Flight::route('POST /validation/inscription/entreprise', function(){

    require 'requetes/validation_inscription_entreprise.php';
    
});

//exécution requete du formulaire d'inscription coté développeur
Flight::route('POST /validation/inscription/developpeur', function(){

    require 'requetes/validation_inscription_developpeur.php';
    
});

//exécution requete du formulaire d'ajout d'une offre coté entreprise
Flight::route('POST /validation/ajout/offre', function(){

    require 'requetes/validation_ajout_offre.php';
    
});







/************* */
// Fin de fichier
/************* */
Flight::start();