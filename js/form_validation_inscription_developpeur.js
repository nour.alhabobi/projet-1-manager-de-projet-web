function checkField( name ) {
    return document.getElementById(name) != undefined && document.getElementById(name).value != "";
}

document
    .getElementById('form_inscription')
    .addEventListener('submit', function( event ){  //Ajoute un evennement à la cible

        var nom_developpeur_ok=false;
        var prenom_ok = false;
        var courriel_ok = false;
        var telephone_ok = false;
        var username_ok=false;
        var mdp_ok=false;

        if( checkField('nom') ) nom_developpeur_ok = true;
        if( checkField('prenom') ) prenom_ok = true;
        if( checkField('courriel') ) courriel_ok = true;
        if( checkField('telephone') )  telephone_ok = true;
        if( checkField('username')) username_ok = true;/* on verifie que le nom est bien indiqué*/
        if(  checkField('mdp') ) mdp_ok = true;/* on verifie que le nom est bien indiqué*/
           
        if( nom_developpeur_ok && prenom_ok && courriel_ok && telephone_ok && username_ok && mdp_ok ) 
            return true;		
        else {

            var msg = "";
            
            if( !nom_developpeur_ok) msg +="Veuillez indiquez votre nom svp\n";
            if( !prenom_ok) msg +="Veuillez indiquez votre prénom svp\n";
            if( !courriel_ok) msg +="Veuillez indiquez votre courriel svp\n";
            if( !telephone_ok) msg +="Veuillez indiquez votre téléphone svp\n";
            if( !username_ok ) msg += "Veuillez indiquer votre login\n";
            if( !mdp_ok) msg +="Veuillez indiquez votre mot de passe\n";
            
            alert (msg);
            
            // Desactive l'evennement de base: ici, submit
            event.preventDefault();

        }

    });