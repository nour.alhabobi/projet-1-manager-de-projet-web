function checkField( name ) {
    return document.getElementById(name) != undefined && document.getElementById(name).value != "";
}

document
    .getElementById('form_inscription')
    .addEventListener('submit', function( event ){  //Ajoute un evennement à la cible

        var nom_entreprise_ok=false;
        var siret_ok = false;
        var username_ok=false;
        var mdp_ok=false;

        if( checkField('nom') ) nom_entreprise_ok = true;
        if( checkField('siret')) siret_ok = true;
        if( checkField('username')) username_ok = true;/* on verifie que le nom est bien indiqué*/
        if(  checkField('mdp') ) mdp_ok = true;/* on verifie que le nom est bien indiqué*/
           
        if( nom_entreprise_ok && siret_ok && username_ok && mdp_ok ) 
            return true;		
        else {

            var msg = "";
            
            if( !nom_entreprise_ok) msg +="Veuillez indiquez votre nom svp\n";
            if( !siret_ok ) msg += "Veuillez indiquer votre siret\n";
            if( !username_ok ) msg += "Veuillez indiquer votre login\n";
            if( !mdp_ok) msg +="Veuillez indiquez votre mot de passe\n";
            
            alert (msg);
            
            // Desactive l'evennement de base: ici, submit
            event.preventDefault();

        }

    });