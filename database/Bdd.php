<?php

// Définitions de constantes pour la connexion à MySQL
class Bdd {

	private $hote;
	private $login;
	private $mdp;
	private $nombd;

	private $connection;

	// constructeur des classes php 
	function __construct( $hote=BDD_HOST, $login=BDD_USER, $mdp=BDD_PASSWORD, $nombd=BDD_NAME) {

		$this->hote = $hote;
		$this->login = $login;
		$this->mdp = $mdp;
		$this->nombd = $nombd;

		$this->connect();

	}

	// Connection au serveur
	private function connect() {

		try {

			$this->connection = new PDO(
				"mysql:host=$this->hote;dbname=$this->nombd",
				$this->login, 
				$this->mdp
			);

		} catch ( Exception $e  ) {
			die ("\n connexion à ".$hote." impossible: ".$e->getMessage());
		}

	}

	function getConnection() {
		return $this->connection;
	}

}
